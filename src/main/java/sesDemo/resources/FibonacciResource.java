package sesDemo.resources;

import com.codahale.metrics.annotation.Timed;
import sesDemo.api.Showing;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Path("/fibonacci")
@Produces(MediaType.APPLICATION_JSON)
public class FibonacciResource {
    private final String template;
    private final String defaultName;
    private final AtomicLong counter;


    public FibonacciResource(String template, String defaultName) {
        this.template = template;
        this.defaultName = defaultName;
        this.counter = new AtomicLong();
    }

    @GET
    @Timed
    public Showing sayHello(@QueryParam("number")  int num) throws Exception {
        List<String> fullcontent = new ArrayList<>();
        try{
            if (num <1 || num >100 ) {
                fullcontent.add("Please enter a number between 1 and 100 and try again like below");
                fullcontent.add("query parameter name is : number");
                throw new Exception("Exception");
            }
        } catch (Exception e) {
            return new Showing(fullcontent);
        }


        fullcontent.add("member-count :  "+ num );
        List<Long> sequence = new ArrayList();
        Map<String,String> map = new HashMap<String,String>();

        long nextnum=1;
        long prevnum=1;
        Long currnum=1L;
        long sum=0;
        for(int i=1;i<=num;i++){
            sum = sum+nextnum;
            sequence.add(currnum);
            nextnum=currnum+prevnum;
            prevnum = currnum;
            currnum=nextnum;
        }
        //sum=sum+nextnum;
        fullcontent.add("Sequence : " +Stream.of(sequence).collect(Collectors.toList()));
        fullcontent.add("total :" + sum);

        return new Showing(fullcontent);
    }

}
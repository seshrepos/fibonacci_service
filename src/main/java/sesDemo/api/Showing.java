package sesDemo.api;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class Showing {
    private List<String> content;


    public Showing( List<String> content) {
        this.content = content;
    }

    @JsonProperty
    public List<String> getContent() {
        return content;
    }

    //@Override
    //public String toString() {
        //return "Showing{"  + content + '\'' + '}';
    //}

}
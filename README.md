# HelloWorld and Fibonacci

How to start the HelloWorld application
---

1. Run `mvn clean install` to build your application
2. Start application with `java -jar target/javatask-1.0-SNAPSHOT.jar server config.yml`
3. To check that your application is running enter url `http://localhost:8080`
4. To check/access fibonacci service send a request http://localhost:8080/fibonacci?number=7
(Service accepts number between 1 & 100).

How to dockerise the application:
--------------------------------

Docker file is provided in the repo. Just run 

docker build . <docker hub repo>/fibonacci:<version tag> (provide docker user id and password)


Currently below docker image is avaliable.
asesha/fibonacci.v1.0.1

Running docker container on docker dektop
----------------------------------------- 

Just pull image  asesha/fibonacci:v1.0.1 and run in docker desktop engine.
Then connect to endpoints as mentioned above with localhost.

Running docker container on Linux/unix servers
-----------------------------------------------

Make sure docker and docker-compose is installed on server. Install if required 
(e.g sudo yum install docker-engine -y ,
     sudo service docker start
	 sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
	 sudo chmod +x /usr/local/bin/docker-compose
	 sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose)

There is docker compose file in the repository which can be copied to the server.
Run command "docker-compose up -d". This starts the docker container and runs in background.

Now to check services connect to http://<ip address or dns name of server>:8080/fibonacci?number=8

or you can use the curl script 
curl --location --request GET 'http://localhost.8080/fibonacci?number=20'


Health Check
---

To see your applications health enter url `http://localhost:8081/healthcheck`

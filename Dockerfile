##################################
# Maven Build stage
##################################
FROM maven:3-jdk-11 as builder
# Create app folder for sources
WORKDIR /build
COPY pom.xml pom.xml
COPY dependency-reduced-pom.xml dependency-reduced-pom.xml

# Download all required dependencies into one layer
RUN mvn -B dependency:go-offline

# Copy source code
COPY src/ src/

# Static Code Analysis and Build application
RUN mvn -B clean install

##################################
# Build Docker Image - Final stage
##################################
FROM adoptopenjdk:11-openj9

WORKDIR /app

# Possibility to set JVM options
ENV JAVA_OPTS=""
# Possibility to set Application options
ENV APP_OPTS=""
EXPOSE 8080/tcp
# Copy executable jar file from the builder image
COPY --from=builder /build/target/javatask-1.0-SNAPSHOT.jar application.jar
COPY config.yml config.yml
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS  -jar application.jar server config.yml $APP_OPTS" ]
